import React, { createContext, useState, useEffect } from "react";
import { api } from "../utils/api";

interface ProductItem {
    privatetitle: string,
    id: string,
    price: string,
    imageUrl: string,
    category: string,
}


interface ProductContextData {

    fullProducts: Array<ProductItem>
    // teste:(x:string)=>void
}

const ProductContext = createContext<ProductContextData | Partial<ProductContextData>>({});

export const ProductProvider: React.FC = ({ children }) => {

    const [fullProducts, setfullProducts] = useState([])

    async function getProducts() {
        try {

            const response = await api.get('products.json');
            const data = Object.entries(response);
            const dataEntries = Object.entries(data[0][1] )
            const filterPrimary:any = dataEntries.map((item: any) =>(item[1]))
            setfullProducts(filterPrimary)

        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        getProducts()
    }, [])

    return (
        <ProductContext.Provider value={{ fullProducts, }}>
            {children}
        </ProductContext.Provider>
    )

}

export const useProductProvider = () => React.useContext(ProductContext)
