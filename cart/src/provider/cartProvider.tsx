import React, { createContext, useState, useEffect } from "react";

interface CartItem {
    title: string
    id: string,
    price: string,
    imageUrl: string,
    category: string,
}
interface CartContextData {

    cart?: Array<any>
    addItem: (x: CartItem) => void
    deleteProduct: (x: any) => void
    updateQtd: (item: any) => void
}

const CartContext = createContext<CartContextData>({} as CartContextData);

export const CartProvider: React.FC = ({ children }) => {

    const [cart, setCart] = useState(Array)


    async function addItem(item: any) {

        let cardLock = cart.map((e: any) => e.title)
        const arrayTransition: any = []
        if (!cardLock.includes(item.title)) {
            setCart([...cart, {
                title: item.title,
                id: item.id,
                price: item.price,
                imageUrl: item.imageUrl,
                category: item.category,
                qtd: '1'
            }])
        } else {
           
        }
    }


    function deleteProduct(item: any) {
        var deleteItem = cart.filter(function (f) { return f !== item })
        setCart(deleteItem)
    }


    function updateQtd(item: any) {

        
        let ids: any = [];
        ids.push(item.id)
        let result = cart.reduce((acc: any, o: any) => acc.concat(ids.includes(o.id)
            ? Object.assign(o, {
                qtd: item.qtd + 1
            }) :
            o), []);
    }

    function totalAmount() {
        let total: any = 0
        cart.map((qtd) => {
            // total = total + qtd.price
        }
        )

    }

    useEffect(() => {
        totalAmount()
    }, [cart])


    return (
        <CartContext.Provider value={{ cart, addItem, deleteProduct, updateQtd }}>
            {children}
        </CartContext.Provider>
    )

}

export const useCartProvider = () => React.useContext(CartContext)
