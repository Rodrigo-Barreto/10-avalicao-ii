import React from 'react';
import Card from "@material-ui/core/Card";
import { Button, CardContent, CardHeader, CardMedia, IconButton, makeStyles, Typography, } from "@material-ui/core";
import { Favorite, FavoriteBorderOutlined } from "@material-ui/icons";
import { useCartProvider } from '../../provider/cartProvider';

const useStyles: any = makeStyles({
  card: {
    borderRadius: 10,
    maxWidth: 230,
    margin: "auto",
    transition: "0.3s",
    boxShadow: "0 8px 40px -12px rgba(0,0,0,0.0)",
    "&:hover": {
      boxShadow: "0 16px 70px -12.125px rgba(0,0,0,0.3)"
    }
  },
}
)

interface Product {
  title: string
  id: string,
  price: string,
  imageUrl: string,
  category: string,
  description:string
}




function CardItem(props: { product: Product }) {
  const classes = useStyles();
  const{ addItem ,updateQtd} = useCartProvider();
  return (

    <Card className={classes.card}>
      <CardMedia
        component="img"
        height="194"
        image={props.product.imageUrl}
        alt="Paella dish"
      />
      <CardHeader
        title={<Typography >{props.product.title}</Typography>}
      action={<IconButton>
          
      </IconButton>}
      />
      <CardContent>


        <h2 className={classes.price}> R$ {props.product.price}</h2 >
        <Typography >
          {props.product.description}
        </Typography>
        <Button onClick={()=>{ addItem(props.product);}}>Add ao carrinho</Button>
      </CardContent>
    </Card>
    // <p>{props.product.title}</p>


  )
}

export default CardItem;
