import { AppBar, Badge, Button, Grid, Icon, IconButton, Toolbar } from "@material-ui/core";
import { Favorite, FavoriteBorder, ShoppingCartOutlined } from "@material-ui/icons";
import './style.css'
import React from "react";


function AppBarCustom() {
    const [open, setOpen] = React.useState(false);
    const [state, setState] = React.useState({ right: false });


    
    return (
  
    <>
  <AppBar>
        <Grid container className='topBar'  >
          <Grid item xs={3} className='logo'>
          </Grid>
          <Grid item xs={6} className='logo' >
            <img src='https://rihappynovo.vtexassets.com/arquivos/logo-rihappy-2d-solzinho-desk.png' alt='logo'  />
          </Grid>
          <Grid item xs={3} className='logo'>
              <IconButton>
              </IconButton>
          </Grid>
        </Grid>



        <Grid container className='topBarDivider'  >
          <Grid item xs={2} className='item1'>
          </Grid>
          <Grid item xs={2} className='item2'>
          </Grid>
          <Grid item xs={2} className='item3'>
          </Grid>
          <Grid item xs={2} className='item4'>
          </Grid>
          <Grid item xs={2} className='item5'>
          </Grid>
          <Grid item xs={2} className='item6'>
          </Grid>
        </Grid>


        
      </AppBar>


    </>
  
    )
  }
  
  export default AppBarCustom;
  