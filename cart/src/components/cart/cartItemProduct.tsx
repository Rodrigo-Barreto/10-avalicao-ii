import { AppBar, Badge, Button, Card, Grid, Icon, IconButton, Toolbar } from "@material-ui/core";
import { Delete, Favorite, FavoriteBorder, ShoppingCartOutlined } from "@material-ui/icons";
import './style.css'
import React from "react";
import { useCartProvider } from "../../provider/cartProvider";


function CartItemProduct(props: { product: any }) { 
   
    const { deleteProduct } = useCartProvider()
    return (
  
    <>
  <Card  className="cardItem">
            <Grid  container   >
                <Grid item xs={3} >
                    <img src={props.product.imageUrl} height='100' />
                </Grid>
                <Grid item xs={6}  >
                    {props.product.title} 
                </Grid>
                <Grid item xs={3}>
                    <IconButton onClick={()=> deleteProduct(props.product)}>
                        <Delete ></Delete>
                    </IconButton>
                    {props.product.qtd}
                </Grid>
            </Grid>
            
        </Card>

    </>
  
    )
  }
  
  export default  CartItemProduct;
  