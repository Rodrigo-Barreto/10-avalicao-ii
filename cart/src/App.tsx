import { Box, Card, Grid } from '@material-ui/core';
import React from 'react';
import './App.css';
import CardItem from './components/card/card';
import AppBarCustom from './components/cart/cartItem';
import CartItemProduct from './components/cart/cartItemProduct';
import item from './components/cart/cartItemProduct';

import { useCartProvider } from './provider/cartProvider';
import { useProductProvider } from './provider/productProvider';






const App: React.FC = () => {

  const { fullProducts } = useProductProvider()
  const { cart } = useCartProvider()
  console.log()
  return (
    <>
      <AppBarCustom></AppBarCustom>
<Card>
<Grid container >
        <Grid item xs={8}>
          <Box sx={{ flexGrow: 1 }}>
            <Grid container spacing={1}>
            </Grid>
          </Box>
          <Box sx={{ flexGrow: 1 }}>
            <Grid container spacing={1}>
              {fullProducts?.map((item: any) =>
                <Grid item md={3} xs={12} sm={6}>
                  <CardItem
                    product={item} />
                </Grid>
              )}
            </Grid>
          </Box>
        </Grid>
        <Grid item xs={4} className='direita'>
          {cart?.map((item: any) =>  <CartItemProduct product={item}></CartItemProduct>)}
        </Grid>
      </Grid>


</Card>


     









    </>

  );
}

export default App;
